import { SalFirebasePage } from './app.po';

describe('sal-firebase App', function() {
  let page: SalFirebasePage;

  beforeEach(() => {
    page = new SalFirebasePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
