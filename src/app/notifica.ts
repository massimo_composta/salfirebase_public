export class Notifica {
    titolo: String;
    contenuto: String;
    destinatario: String;
    tipoDestinatario: String = "token";
}
