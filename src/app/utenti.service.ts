import {Injectable, Inject} from '@angular/core';
import {FirebaseApp} from "angularfire2";
import * as firebase from 'firebase';
import Database = firebase.database.Database;
import {Utente} from "./utente";
import {Observable} from "rxjs";

@Injectable()
export class UtentiService {
  private database: Database;

  private PATH = "users/";

  constructor(@Inject(FirebaseApp) private firebaseApp: firebase.app.App) {
    this.database = firebase.database(firebaseApp);
  }

  aggiungiUtente(utente: Utente): firebase.Promise<void>{
    console.log("inserisco utente " + JSON.stringify(utente));
    return this.database.ref(this.PATH + utente.id).set({
      id: utente.id,
      username: utente.username,
      email: utente.email,
      dataNascita: utente.dataNascita.getTime()
    });
  }


  getRef() {
    return this.database.ref(this.PATH)
  }
}
