import {Component, OnInit, Inject} from '@angular/core';

import {NotificheService} from "../notifiche.service";

@Component({
  selector: 'app-area-notifiche',
  templateUrl: './area-notifiche.component.html',
  styleUrls: ['./area-notifiche.component.css']
})
export class AreaNotificheComponent implements OnInit {

  private notifiche = [];

  constructor(private notificheService: NotificheService) {

  }

  ngOnInit() {
     this.notificheService.getMessaging().onMessage((payload) => {
      console.log("Message received. ", payload);
      this.notifiche.push(payload.notification);
    });
  }
}
