export class Utente {
  id: String;
  username: String;
  email: String;
  dataNascita: Date;
}
