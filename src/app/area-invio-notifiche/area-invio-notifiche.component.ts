import {Component, OnInit, Input} from '@angular/core';
import {NotificheService} from "../notifiche.service";
import {Notifica} from "../notifica";

@Component({
    selector: 'app-area-invio-notifiche',
    templateUrl: './area-invio-notifiche.component.html',
    styleUrls: ['./area-invio-notifiche.component.css']
})
export class AreaInvioNotificheComponent implements OnInit {
    private notifica: Notifica = new Notifica();

    constructor(private notificheService : NotificheService) {
    }

    ngOnInit() {
    }

    inviaNotifica() {
        this.notificheService.inviaNotifica(this.notifica).subscribe();
    }

    resetForm() {
        this.notifica = new Notifica();
    }
}
