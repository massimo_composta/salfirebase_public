/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { NotificheService } from './notifiche.service';

describe('NotificheService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NotificheService]
    });
  });

  it('should ...', inject([NotificheService], (service: NotificheService) => {
    expect(service).toBeTruthy();
  }));
});
