import {Injectable, Inject} from '@angular/core';
import {Http, Headers, RequestOptions, Response} from "@angular/http";
import {FirebaseApp} from "angularfire2";
import Messaging = firebase.messaging.Messaging;
import * as firebase from 'firebase'
import {Notifica} from "./notifica";
import {Observable} from "rxjs";
@Injectable()
export class NotificheService {
    private messaging: Messaging;
    private serverKey: string;


    constructor(private http: Http,
                @Inject(FirebaseApp) private firebaseApp: firebase.app.App) {
        this.messaging = firebase.messaging(this.firebaseApp);
        this.messaging.requestPermission().then(() => {
            console.log("Notification permission granted.");
        }).catch((err) => {
            console.log("Unable to get permission to notify.", err);
        });
        this.serverKey = "AAAAMGIn8E8:APA91bFhVmx8SFxsaOtj-EUSMNWxYFS7uMOrB3H0sP5_mpeE60og-Mb_Z0GmsOaHUj4cBVckdE19oJaGgiuPYNdnbemFbiQAbvhcV9cSoe-cZQsIFCOaqHlxeohxmjQ4Goa5wN9ukVVO";
    }

    getToken(): firebase.Promise<any> {
        return this.messaging.getToken();
    }

    inviaNotifica(notifica: Notifica) {
        let headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'key=' + this.serverKey
        });

        let options = new RequestOptions({headers: headers});
        let content: any = {
            "notification": {
                "title": notifica.titolo,
                "body": notifica.contenuto
            }
        };

        if (notifica.tipoDestinatario == 'token') {
            content.to = notifica.destinatario;
        } else if (notifica.tipoDestinatario == 'topic') {
            content.to = "topics/" + notifica.destinatario;
        }

        return this.http.post("https://fcm.googleapis.com/fcm/send", content, options).map((res: Response) => {
            res.json()
        }).catch((error: any) => Observable.throw(error.json().error || "Errore nell'invio della notifica"));
    };

    registraTopic(topic: String, token: String) {
        let headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'key=' + this.serverKey
        });
        let options = new RequestOptions({headers: headers});

        return this.http.post("https://iid.googleapis.com/iid/v1/" + token + "/rel/topics/" + topic, {}, options).map((res: Response) => {
            res.json()
        }).catch((error: any) => Observable.throw(error.json().error || "Errore nella registrazione del topic"));

    }

    getMessaging() {
        return this.messaging;
    }
}
