import {Component, OnInit, ViewChild} from '@angular/core';
import {ChatboxComponent} from "../chatbox/chatbox.component";


@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['chat.component.css']
})
export class ChatComponent implements OnInit {

  @ViewChild('chatbox') private chatBox: ChatboxComponent;

  ngOnInit(): void {

  }

  onConfigSave(token: string) {
    this.chatBox.setTokenDestinatario(token);
  }
}
