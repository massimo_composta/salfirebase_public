import {Component, OnInit, ViewChild, ElementRef, AfterViewChecked} from '@angular/core';
import {NotificheService} from "../notifiche.service";

@Component({
  selector: 'app-chatbox',
  templateUrl: './chatbox.component.html',
  styleUrls: ['./chatbox.component.css']
})
export class ChatboxComponent implements OnInit, AfterViewChecked {
  private messaggi = [{
    "content": "Messaggio 1",
    "source": "in"
  }, {
    "content": "Messaggio 2",
    "source": "in"
  }, {
    "content": "Messaggio 3",
    "source": "in"
  }, {
    "content": "Messaggio 4",
    "source": "in"
  }, {
    "content": "Messaggio 5",
    "source": "out"
  }];
  @ViewChild('chatBox') private chatBox: ElementRef;
  private token : string;

  private nuovoMessaggio: string;
  //private token: string;
  constructor(private notificheService: NotificheService) {
  }

  ngOnInit() {
    this.notificheService.getToken().then((token)=> {
      this.token = token;
    });
    this.scrollToBottom();
  }

  ngAfterViewChecked() {
    this.scrollToBottom();
  }

  inviaMessaggio() {
    this.messaggi.push({
      "content": this.nuovoMessaggio,
      "source": "out"
    });
    this.nuovoMessaggio = "";
  }

  scrollToBottom(): void {
    try {
      this.chatBox.nativeElement.scrollTop = this.chatBox.nativeElement.scrollHeight;
    } catch (err) {
    }
  };

  setTokenDestinatario(token: string) {
    console.log(token);
  }
}
