import { Component, OnInit } from '@angular/core';
import {NotificheService} from "../notifiche.service";

@Component({
  selector: 'app-info-notifiche',
  templateUrl: './info-notifiche.component.html',
  styleUrls: ['./info-notifiche.component.css']
})
export class InfoNotificheComponent implements OnInit {
  private token: String;

  constructor(private notificheService: NotificheService) { }

  ngOnInit() {
    this.notificheService.getToken().then((token)=> {
      this.token = token;
    });
  };

  registraTopic(topic: String){
    this.notificheService.registraTopic(topic, this.token).subscribe();
  }

}
