import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {MaterialModule} from '@angular/material'
import {AppComponent} from './app.component';
import {AreaNotificheComponent} from './area-notifiche/area-notifiche.component';
import {AngularFireModule} from "angularfire2";
import {FlexLayoutModule} from '@angular/flex-layout';
import {AreaInvioNotificheComponent} from './area-invio-notifiche/area-invio-notifiche.component'
import {NotificheService} from "./notifiche.service";
import {InfoNotificheComponent} from './info-notifiche/info-notifiche.component';
import {RouterModule, Routes} from "@angular/router";
import {FCMComponent} from './fcm/fcm.component';
import {DatabaseComponent} from './database/database.component';
import {RemoteConfigurationComponent} from './remote-configuration/remote-configuration.component';
import {Md2Module} from "md2";
import {UtentiService} from "./utenti.service";
import {ListaUtentiComponent} from './lista-utenti/lista-utenti.component';
import {ChatComponent} from './chat/chat.component';
import {ChatboxComponent} from './chatbox/chatbox.component';
import {ChatConfiguratorComponent} from './chat-configurator/chat-configurator.component';
import {NuovoUtenteComponent} from './nuovo-utente/nuovo-utente.component';

export const config = {
  apiKey: "AIzaSyD3k66MslU1SF5NXcHOsLRkJQRCXOWaU0A",
  authDomain: "sal-firebase.firebaseapp.com",
  databaseURL: "https://sal-firebase.firebaseio.com",
  storageBucket: "sal-firebase.appspot.com",
  messagingSenderId: "207805214799"
};
const appRoutes: Routes = [
  {path: 'fcm', component: FCMComponent},
  {path: 'database', component: DatabaseComponent},
  {path: 'remote-configuration', component: RemoteConfigurationComponent},
  {path: 'chat', component: ChatComponent}

];
@NgModule({
  declarations: [
    AppComponent,
    AreaNotificheComponent,
    AreaInvioNotificheComponent,
    InfoNotificheComponent,
    FCMComponent,
    DatabaseComponent,
    RemoteConfigurationComponent,

    ListaUtentiComponent,
    ChatComponent,
    ChatboxComponent,
    ChatConfiguratorComponent,
    NuovoUtenteComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    MaterialModule,
    AngularFireModule.initializeApp(config),
    FlexLayoutModule,
    RouterModule.forRoot(appRoutes),
    Md2Module
  ],
  providers: [NotificheService, UtentiService],
  bootstrap: [AppComponent],
  entryComponents: [NuovoUtenteComponent]
})
export class AppModule {
}
