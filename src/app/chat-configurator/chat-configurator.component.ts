import {Component, OnInit, Output, EventEmitter} from '@angular/core';


@Component({
  selector: 'app-chat-configurator',
  templateUrl: './chat-configurator.component.html',
  styleUrls: ['./chat-configurator.component.css']
})
export class ChatConfiguratorComponent implements OnInit {

  @Output() onConfigSave = new EventEmitter<string>();
  private token: string;
  constructor() {
  }

  ngOnInit() {
  }

  salva() {
    this.onConfigSave.emit(this.token);
  }
}
