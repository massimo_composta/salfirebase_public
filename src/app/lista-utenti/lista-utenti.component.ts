import {Component, OnInit} from '@angular/core';
import {MdDialog} from "@angular/material";
import {NuovoUtenteComponent} from "../nuovo-utente/nuovo-utente.component";
import {Utente} from "../utente";
import {UtentiService} from "../utenti.service";

@Component({
  selector: 'app-lista-utenti',
  templateUrl: './lista-utenti.component.html',
  styleUrls: ['./lista-utenti.component.css']
})
export class ListaUtentiComponent implements OnInit {
  utenti: Utente[] = [];
  constructor(private dialog: MdDialog, private utentiService: UtentiService) {
  }

  ngOnInit() {
    let utentiRef = this.utentiService.getRef();

    utentiRef.on('value', (snapshot) => {
      this.utenti = [];
      let utenti = snapshot.val();
      Object.keys(utenti).forEach((id, index) => {
        this.utenti[index] = utenti[id];
      })
    });
  }

  apriPopupNuovoUtente() {
    console.log("apro dialog");
    this.dialog.open(NuovoUtenteComponent);
  }
}
