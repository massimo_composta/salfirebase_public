/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { FCMComponent } from './fcm.component';

describe('FCMComponent', () => {
  let component: FCMComponent;
  let fixture: ComponentFixture<FCMComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FCMComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FCMComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
