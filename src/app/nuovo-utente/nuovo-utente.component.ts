import { Component, OnInit } from '@angular/core';
import {Utente} from "../utente";
import {UtentiService} from "../utenti.service";

@Component({
  selector: 'app-nuovo-utente',
  templateUrl: './nuovo-utente.component.html',
  styleUrls: ['./nuovo-utente.component.css']
})
export class NuovoUtenteComponent implements OnInit {
  utente: Utente = new Utente();
  constructor(private utentiService: UtentiService) { }

  ngOnInit() {
  }

  salvaUtente(){
    this.utentiService.aggiungiUtente(this.utente);
  }

}
